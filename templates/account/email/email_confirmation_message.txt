{% if user.first_name %}
Hi {{ user.first_name }},
{% else %}
Hi there,
{% endif %}

You have requested login access to Ezpresso website. To continue visit the link below.
{{ activate_url }}

If you didn't try to login to Ezpresso please ignore this message. We apologise for inconvenience.

Sincerely,
The Ezpressor Team

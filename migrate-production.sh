#!/usr/bin/env bash

mkdir -p media/products && unzip -o -q saleor/fixtures/products.zip -d media/products
echo "DROP SCHEMA public CASCADE; CREATE SCHEMA public;" | python manage.py dbshell
python manage.py migrate --database=default

function run() {
  echo "Running fixture: $1"
  python manage.py loaddata saleor/fixtures/$1.json
}

run products
run users
run sites
run shippings

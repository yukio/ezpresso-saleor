"""
WSGI config for ezpresso project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.conf import settings
from django.core.wsgi import get_wsgi_application
from raven.middleware import Sentry
from whitenoise.django import DjangoWhiteNoise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "saleor.settings")

if settings.DEBUG:
    application = DjangoWhiteNoise(get_wsgi_application())
else:
    application = Sentry(get_wsgi_application())


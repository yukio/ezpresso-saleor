from django.conf.urls import url

from saleor.blog.views import ListPostView, DetailPostView
from . import views


urlpatterns = [
    url(r"^$", ListPostView.as_view(), name="list"),
    url(r"^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$", DetailPostView.as_view(), name="post"),
]

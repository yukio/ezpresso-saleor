from django.db.models import Q
from django.http import Http404
from django.views.generic import DetailView, ListView

from saleor.blog.models import Post
from saleor import settings


class ListPostView(ListView):
    model = Post
    template_name = "blog/list.html"
    search_parameter = "q"
    paginate_by = settings.PAGINATE_BY

    def get_current_section(self):
        return "all"

    def get_context_data(self, **kwargs):
        context = super(ListPostView, self).get_context_data(**kwargs)
        context.update({
            "current_section": self.get_current_section(),
            "search_term": self.search_term()
        })
        return context

    def search_term(self):
        return self.request.GET.get(self.search_parameter)

    def search(self, posts):
        q = self.search_term()
        if q:
            posts = posts.filter(
                Q(title__icontains=q) |
                Q(teaser_html__icontains=q) |
                Q(content_html__icontains=q)
            )
        return posts


class DetailPostView(DetailView):
    model = Post
    template_name = "blog/post.html"
    pk_url_kwarg = "post_pk"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

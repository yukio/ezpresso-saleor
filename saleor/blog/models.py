from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, pgettext_lazy
from draceditor.models import DraceditorField

from saleor.product.models import Category


@python_2_unicode_compatible
class Post(models.Model):
    title = models.CharField(max_length=200, verbose_name=_("title"))
    slug = models.SlugField()
    cover_image = models.CharField(verbose_name=_("Cover image URL"), max_length=1024)
    summary = models.TextField(verbose_name=_("summary"))
    description = DraceditorField(verbose_name=_("content"))

    post_date = models.DateField(auto_now_add=True, verbose_name=_("post date"))
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                                  verbose_name=_("author"))

    categories = models.ManyToManyField(
        Category, verbose_name=pgettext_lazy('Post field', 'categories'),
        related_name='post')

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        ordering = ['-post_date']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug,
            'year': '%04d' % self.post_date.year,
            'month': '%02d' % self.post_date.month,
            'day': '%02d' % self.post_date.day,
        }

        return reverse('blog:post', kwargs=kwargs)

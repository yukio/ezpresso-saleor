from raven.base import Raven


def report_error_to_sentry(e=None):
    if not e:
        e = Exception('Note to team: Something goes wrong here')
    Raven.captureException(e)

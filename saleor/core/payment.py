from django.http import HttpResponseRedirect
from payments import PaymentStatus, RedirectNeeded, PaymentError, FraudStatus
from payments.core import BasicProvider

from saleor.core.forms import NoProviderForm
try:
    # For Python 3.0 and later
    from urllib.error import URLError
    from urllib.parse import urlencode
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import URLError
    from urllib import urlencode


class NoProvider(BasicProvider):
    def get_form(self, payment, data=None):
        if payment.status == PaymentStatus.WAITING:
            payment.change_status(PaymentStatus.INPUT)
        ref = 'P{0:06d}'.format(payment.order.id)
        form = NoProviderForm(ref=ref, data=data, hidden_inputs=False, provider=self, payment=payment)
        if form.is_valid():
            payment.change_status(PaymentStatus.PREAUTH)
            payment.change_fraud_status(FraudStatus.UNKNOWN)
            raise RedirectNeeded(payment.get_success_url())
        return form

    def process_data(self, payment, request):
        if payment.status in [PaymentStatus.CONFIRMED, PaymentStatus.PREAUTH]:
            return HttpResponseRedirect(payment.get_success_url())
        return HttpResponseRedirect(payment.get_failure_url())

    def capture(self, payment, amount=None):
        payment.change_status(PaymentStatus.CONFIRMED)
        return amount

    def release(self, payment):
        return None

    def refund(self, payment, amount=None):
        return amount or 0

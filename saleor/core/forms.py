from payments.forms import PaymentForm


class NoProviderForm(PaymentForm):
    def __init__(self, ref, data, hidden_inputs, provider, payment):
        self.ref = ref
        super(NoProviderForm, self).__init__(data=data, hidden_inputs=False, provider=self, payment=payment)

    def clean(self):
        cleaned_data = super(NoProviderForm, self).clean()
        return cleaned_data

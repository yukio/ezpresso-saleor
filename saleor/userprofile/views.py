import datetime

from allauth.account.utils import logout_on_password_change
from allauth.account.views import login as allauth_login
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template.response import TemplateResponse
from django.utils.translation import pgettext
from django.views.generic import FormView

from saleor.models import SupportType
from saleor.product.models import Product
from saleor.userprofile.models import SupportRequest, User
from .forms import ChangePasswordForm, get_address_form, SupportForm
from .forms import (logout_on_password_change)
from ..cart.utils import find_and_assign_anonymous_cart


@login_required
def details(request):
    password_form = get_or_process_password_form(request)
    ctx = {'addresses': request.user.addresses.all(),
           'orders': request.user.orders.prefetch_related('groups__items'),
           'change_password_form': password_form}

    return TemplateResponse(request, 'userprofile/details.html', ctx)


def get_or_process_password_form(request):
    form = ChangePasswordForm(data=request.POST or None, user=request.user)
    if form.is_valid():
        form.save()
        logout_on_password_change(request, form.user)
        messages.success(request, pgettext(
            'Storefront message', 'Password successfully changed.'))
    return form


@login_required
def address_edit(request, pk):
    address = get_object_or_404(request.user.addresses, pk=pk)
    address_form, preview = get_address_form(
        request.POST or None, instance=address,
        country_code=address.country.code)
    if address_form.is_valid() and not preview:
        address_form.save()
        message = pgettext('Storefront message', 'Address successfully updated.')
        messages.success(request, message)
        return HttpResponseRedirect(reverse('profile:details') + '#addresses')
    return TemplateResponse(
        request, 'userprofile/address-edit.html',
        {'address_form': address_form})


@login_required
def address_delete(request, pk):
    address = get_object_or_404(request.user.addresses, pk=pk)
    if request.method == 'POST':
        address.delete()
        messages.success(
            request,
            pgettext('Storefront message', 'Address successfully deleted.'))
        return HttpResponseRedirect(reverse('profile:details') + '#addresses')
    return TemplateResponse(
        request, 'userprofile/address-delete.html', {'address': address})


class SupportView(FormView):
    template_name = 'userprofile/support.html'
    form_class = SupportForm

    def get_context_data(self, **kwargs):
        initial = self.get_initial()
        if initial:
            form = SupportForm(initial=initial)
        else:
            form = self.get_form(self.get_form_class())
        return {"form": form}

    def get_initial(self):
        # previous_data = self.request.POST
        # if not previous_data:
        #     initial = {}
        #     user = self.request.user
        #     if user.is_authenticated:
        #         initial['name'] = user.first_name + ' ' + user.last_name
        #         initial['phone'] = user.get_phone_number()
        #         initial['email'] = user.email
        #     return initial
        # else:
        #     return None
        initial = {}
        if 'product' in self.request.GET:
            product_id = self.request.GET['product']
            product = Product.objects.get(id=product_id)
            if product:
                initial['category'] = SupportType.ABOUT_PRODUCT
                initial['subject'] = 'About "%s"' % (product.name, )
        user = self.request.user
        if user.is_authenticated:
            initial['name'] = user.first_name + ' ' + user.last_name
            initial['phone'] = user.get_phone_number()
            initial['email'] = user.email

        return initial


    def form_valid(self, form, **kwargs):
        data = form.cleaned_data
        support_request = SupportRequest()
        support_request.name = data['name']
        support_request.phone = data['phone']
        support_request.email = data['email']
        support_request.category = data['category']
        support_request.message = data['message']
        support_request.datetime = datetime.datetime.now()

        # If user has not logged in, search for the one that has the same email address
        if self.request.user.is_authenticated():
            user = self.request.user
        else:
            user = User.objects.filter(email=support_request.email).first()

        if user is not None and support_request.email == user.email:
            support_request.user = user

        support_request.save()
        return render(self.request, 'userprofile/support-confirmation.html')


login = find_and_assign_anonymous_cart()(allauth_login)

# encoding: utf-8
from __future__ import unicode_literals

from allauth.account.adapter import get_adapter
from django import forms
from django.conf import settings
from django.contrib.auth import forms as django_forms, update_session_auth_hash

from saleor.forms import ErrorMixin
from saleor.models import SupportType
from .i18n import AddressMetaForm, get_address_form_class


def get_address_form(data, country_code, initial=None, instance=None, **kwargs):
    country_form = AddressMetaForm(data, initial=initial)
    preview = False

    if country_form.is_valid():
        country_code = country_form.cleaned_data['country']
        preview = country_form.cleaned_data['preview']

    address_form_class = get_address_form_class(country_code)

    if not preview and instance is not None:
        address_form_class = get_address_form_class(
            instance.country.code)
        address_form = address_form_class(
            data, instance=instance,
            **kwargs)
    else:
        initial_address = (
            initial if not preview
            else data.dict() if data is not None else data)
        address_form = address_form_class(
            not preview and data or None,
            initial=initial_address,
            **kwargs)
    return address_form, preview


class ChangePasswordForm(django_forms.PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password1'].user = self.user
        self.fields['old_password'].widget.attrs['placeholder'] = ''
        self.fields['new_password1'].widget.attrs['placeholder'] = ''
        del self.fields['new_password2']

    def save(self):
        get_adapter().set_password(self.user, self.cleaned_data['password1'])


def logout_on_password_change(request, user):
    if (update_session_auth_hash is not None and
            not settings.LOGOUT_ON_PASSWORD_CHANGE):
        update_session_auth_hash(request, user)


class SupportForm(ErrorMixin, forms.Form):

    name = forms.CharField(
        required=True,
        max_length=100,
        error_messages={
            'required': 'Please enter your name'
        }
    )

    email = forms.EmailField(
        required=True,
        max_length=100,
        error_messages={
            'required': 'Please enter your email address'
        }
    )

    phone = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(
            attrs={
                "maxlength": 20
            }
        ),
    )

    category = forms.ChoiceField(
        required=True,
        choices=SupportType.as_choices,
        widget=forms.Select()
    )

    subject = forms.CharField(
        required=False,
        max_length=1023,
    )

    message = forms.CharField(
        required=True,
        min_length=50,
        max_length=1000000,
        error_messages={
            'required': 'Please leave a message'
        },
        widget=forms.Textarea(
            attrs={
                'cols': 80,
                'rows': 10,
                "minlength": 50,
                "maxlength": 1000000
            }
        )
    )

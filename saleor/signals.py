from __future__ import print_function

import threading

from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import Context

from saleor.models import SupportType
from saleor.userprofile.models import SupportRequest

from_email = settings.DEFAULT_FROM_EMAIL
site_url = settings.SITE_URL
support_email = settings.SUPPORT_EMAIL

__all__ = ['render_and_send_email']


class SendEmailThread(threading.Thread):
    def __init__(self, email, **kwargs):
        self.email = email
        super(SendEmailThread, self).__init__(**kwargs)

    def run(self):
        self.email.send(fail_silently=False)


class EzpressoEmailAdapter(DefaultAccountAdapter):
    def send_mail(self, template_prefix, email, context):
        email = self.render_mail(template_prefix, email, context)
        send_email_thread = SendEmailThread(email)
        send_email_thread.start()

    def get_from_email(self):
        return from_email


def render_and_send_email(template_prefix, email, context):
    adapter = EzpressoEmailAdapter()
    email = adapter.render_mail(template_prefix, email, context)
    send_email_thread = SendEmailThread(email)
    send_email_thread.start()


@receiver(post_save, sender=SupportRequest)
def support_request_handler(sender, **kwargs):
    sr = kwargs['instance']
    created = kwargs['created']
    raw = kwargs['raw']

    if not created or raw:
        return

    template = 'account/email/support_request'
    category = SupportType.get_name(int(sr.category))
    datetime = sr.datetime.strftime("%Y/%m/%d %H:%M:%S")
    context = Context({'sr': sr, 'siteurl': site_url, 'category': category, 'datetime': datetime})
    render_and_send_email(template, support_email, context)

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post-list'),
    url(r'^add/$', views.post_add, name='post-add'),
    url(r'^(?P<pk>\d+)/$', views.post_detail, name='post-detail'),
    url(r'^(?P<pk>\d+)/delete/$', views.post_delete, name='post-delete'),
]

from __future__ import unicode_literals

from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.utils.translation import pgettext_lazy

from saleor.blog.models import Post
from saleor.dashboard.blog.forms import PostForm
from ...core.utils import get_paginator_items


@staff_member_required
def post_list(request):
    posts = Post.objects.all()
    posts = get_paginator_items(posts, 30, request.GET.get('page'))
    ctx = {'posts': posts}
    return TemplateResponse(request, 'dashboard/blog/post_list.html', ctx)


def post_edit(request, post):
    form = PostForm(request, instance=post)
    if form.is_valid():
        post = form.save()
        msg = pgettext_lazy(
            'Dashboard message', '%(post_name)s post saved') % {
                'post_name': post}
        messages.success(request, msg)
        return redirect('dashboard:post-list')
    ctx = {'post_form': form, 'post': post}
    return TemplateResponse(request, 'dashboard/blog/post_form.html', ctx)


@staff_member_required
def post_add(request):
    post = Post()
    return post_edit(request, post)


@staff_member_required
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return post_edit(request, post)


@staff_member_required
def post_delete(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        post.delete()
        messages.success(
            request,
            pgettext_lazy(
                'Dashboard message', '%(post)s successfully deleted') % {
                    'post': post})
        return redirect('dashboard:post-list')
    ctx = {'post': post}
    return TemplateResponse(request, 'dashboard/blog/post_delete.html', ctx)

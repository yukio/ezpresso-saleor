from __future__ import unicode_literals

from django import forms
from draceditor.fields import DraceditorFormField

from saleor.blog.models import Post


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        exclude = []
        widgets = {
            'content': DraceditorFormField()
        }

    def __init__(self, request, *args, **kwargs):
        initial = kwargs.get('initial', {})
        instance = kwargs.get('instance')
        if instance and instance.id is None and not initial.get('author'):
            initial['author'] = request.user
        kwargs['initial'] = initial
        super(PostForm, self).__init__(data=request.POST or None, *args, **kwargs)

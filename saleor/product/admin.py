from django.contrib import admin

from models import *

admin.site.register(Category)
admin.site.register(ProductClass)
admin.site.register(ProductVariant)
admin.site.register(ProductAttribute)
admin.site.register(ProductImage)
admin.site.register(VariantImage)

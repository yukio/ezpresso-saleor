import string
from operator import itemgetter
from django.db import models
from django.contrib.auth.tokens import default_token_generator

from saleor.userprofile.models import User


class MagicChoices(object):
    """
    Base class for `automatic' Django enum classes,
    extended to support ordering and custom value strings

    Ordering:  `_ORDER' = [value list, ...]
    Strings:   `_STRINGS' = {value: string, ...}
    """

    @classmethod
    def get_name(cls, value):
        for k, v in cls.__dict__.iteritems():
            if not k.startswith('_') and v == value:
                return cls.convert_enum_string(k)
        raise KeyError('no matching key for value: ' + value)

    @staticmethod
    def convert_enum_string(s):
        return string.capwords(s.replace('_', ' '), ' ')

    @classmethod
    def reverse_items(cls):
        return {v: k for k, v in cls.__dict__.iteritems()
                if not k.startswith('_')}

    @classmethod
    def as_choices(cls):
        if hasattr(cls, '_ORDER'):
            _reverse = cls.reverse_items()
            _items = [(_reverse[v], v) for v in cls._ORDER]
        else:
            _items = sorted(cls.__dict__.iteritems(), key=itemgetter(1))

        if hasattr(cls, '_STRINGS'):
            return tuple(
                (v, cls._STRINGS[v]) for k, v in _items
                if not k.startswith('_'))
        else:
            return tuple(
                (v, cls.convert_enum_string(k)) for k, v in _items
                if not k.startswith('_'))


class SupportType(MagicChoices):
    # only used for the guest
    GENERAL_ENQUIRY = 1
    TECH_SUPPORT = 2
    ORDER_RELATED = 3
    REFUNDS_CANCELLATIONS = 4
    ABOUT_PRODUCT = 5

    _STRINGS = {
        TECH_SUPPORT: 'Tech support',
        GENERAL_ENQUIRY: 'General enquiry',
        REFUNDS_CANCELLATIONS: 'Refunds and cancellations',
        ORDER_RELATED: 'Questions about my order',
        ABOUT_PRODUCT: 'Questions about a product'
    }
    _DEFAULT = GENERAL_ENQUIRY

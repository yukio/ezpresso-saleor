from __future__ import unicode_literals

import ast
import os.path

import dj_database_url
import dj_email_url
import raven
from django.contrib.messages import constants as messages
import django_cache_url

DEBUG = ast.literal_eval(os.environ.get('DEBUG', 'True'))
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_DIR = os.path.dirname(os.path.dirname(__file__))

SITE_ID = 1

PROJECT_ROOT = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))

# a setting to determine whether we are running on OpenShift
PRODUCTION = False
if os.environ.has_key('IS_PRODUCTION'):
    PRODUCTION = True

# SECURITY WARNING: don't run with debug turned on in production!
if PRODUCTION:
    DEBUG = False
    ALLOWED_HOSTS = ['www.ezpresso.co.nz', '139.59.102.50']
    CSRF_TRUSTED_ORIGINS = ['ezpresso.co.nz']
else:
    DEBUG = True
    ALLOWED_HOSTS = ['*']
    CSRF_TRUSTED_ORIGINS = ['*']

ROOT_URLCONF = 'saleor.urls'

WSGI_APPLICATION = 'saleor.wsgi.application'

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
MANAGERS = ADMINS
INTERNAL_IPS = os.environ.get('INTERNAL_IPS', '127.0.0.1').split()

CACHES = {'default': django_cache_url.config()}

if os.getenv('REDIS_HOST'):
    CACHES['default'] = {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '{0}:{1}'.format(os.getenv('REDIS_HOST'), os.getenv('REDIS_PORT')),
    }

if PRODUCTION:
    # os.environ['DB_*'] variables can be used with databases created
    # with rhc-ctl-app (see /README in this git repo)
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': os.environ['POSTGRESQL_DB_NAME'],
            'USER': os.environ['POSTGRESQL_DB_USERNAME'],
            'PASSWORD': os.environ['POSTGRESQL_DB_PASSWORD'],
            'HOST': os.environ['POSTGRESQL_DB_HOST'],
            'PORT': os.environ['POSTGRESQL_DB_PORT'],
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'yfukuzaw',
            'USER': 'yfukuzaw',
            'PASSWORD': '',
            'HOST': 'localhost',
            'PORT': '5433',
        }
    }

TIME_ZONE = 'Pacific/Auckland'
LANGUAGE_CODE = 'en-nz'
LOCALE_PATHS = [os.path.join(PROJECT_ROOT, 'locale')]
USE_I18N = True
USE_L10N = True
USE_TZ = True

ACCOUNT_ADAPTER = 'saleor.signals.EzpressoEmailAdapter'
ACCOUNT_EMAIL_SUBJECT_PREFIX = '[Ezpresso.co.nz] '

if PRODUCTION:
    SITE_URL = os.getenv('SITE_URL', 'https://www.ezpresso.co.nz/')
else:
    SITE_URL = os.getenv('SITE_URL', 'http://127.0.0.1/')
SITE_URL_BARE = SITE_URL[:-1]
assert SITE_URL.endswith('/'), 'SITE_URL must have trailing /'

EMAIL_CONFIG = 'smtp.zoho.com:info@ezpresso.co.nz:%G88$2KV#ZgpG^n*8sv2:587'
DEFAULT_FROM_EMAIL = 'info@ezpresso.co.nz'
SUPPORT_EMAIL = 'info@ezpresso.co.nz'

email_host, email_user, email_pass, email_port = EMAIL_CONFIG.split(':', 3)
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = email_host
EMAIL_HOST_USER = email_user
EMAIL_HOST_PASSWORD = str(email_pass)
EMAIL_TIMEOUT = 10  # hopefully this is enough...
EMAIL_PORT = int(email_port)
EMAIL_USE_TLS = True

ORDER_FROM_EMAIL = os.getenv('ORDER_FROM_EMAIL', DEFAULT_FROM_EMAIL)

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

STATICFILES_FINDERS = [
    # 'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
]
STATICFILES_DIRS = (
    ('assets', os.path.join(PROJECT_ROOT, 'saleor', 'static', 'assets')),
    ('images', os.path.join(PROJECT_ROOT, 'saleor', 'static', 'images')),
    ('dashboard', os.path.join(PROJECT_ROOT, 'saleor', 'static', 'dashboard')),
    ('fonts', os.path.join(PROJECT_ROOT, 'saleor', 'static', 'fonts')),
    ('js', os.path.join(PROJECT_ROOT, 'saleor', 'static', 'js')),
    ('placeholders', os.path.join(PROJECT_ROOT, 'saleor', 'static', 'placeholders')),
)

STATIC_ROOT = os.path.join(DATA_DIR, 'static')
MEDIA_ROOT = os.path.join(DATA_DIR, 'media')

context_processors = [
    'django.contrib.auth.context_processors.auth',
    'django.template.context_processors.debug',
    'django.template.context_processors.i18n',
    'django.template.context_processors.media',
    'django.template.context_processors.static',
    'django.template.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.template.context_processors.request',
    'saleor.core.context_processors.default_currency',
    'saleor.core.context_processors.categories',
    'saleor.cart.context_processors.cart_counter',
    'saleor.core.context_processors.search_enabled',
    'saleor.site.context_processors.settings',
    'saleor.core.context_processors.webpage_schema',
    'social_django.context_processors.backends',
    'social_django.context_processors.login_redirect',
]

loaders = [
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # TODO: this one is slow, but for now need for mptt?
    'django.template.loaders.eggs.Loader']

if not DEBUG:
    loaders = [('django.template.loaders.cached.Loader', loaders)]

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [os.path.join(PROJECT_ROOT, 'templates')],
    'OPTIONS': {
        'debug': DEBUG,
        'context_processors': context_processors,
        'loaders': loaders,
        'string_if_invalid': '<< MISSING VARIABLE "%s" >>' if DEBUG else ''}}]

# Make this unique, and don't share it with anybody.
# SECRET_KEY = os.environ.get('SECRET_KEY')
SECRET_KEY = 'xu-@r3wx&3wj!btd$k1#!6)d-x%5f^o0hhf#9^k+wm(_y(40(_'

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'babeldjango.middleware.LocaleMiddleware',
    'saleor.core.middleware.DiscountMiddleware',
    'saleor.core.middleware.GoogleAnalytics',
    'saleor.core.middleware.CountryMiddleware',
    'saleor.core.middleware.CurrencyMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
]

INSTALLED_APPS = [
    # External apps that need to go before django's
    'storages',

    # Django modules
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.postgres',

    # Local apps
    'saleor.userprofile',
    'saleor.discount',
    'saleor.product',
    'saleor.cart',
    'saleor.checkout',
    'saleor.core',
    'saleor.graphql',
    'saleor.order',
    'saleor.dashboard',
    'saleor.shipping',
    'saleor.search',
    'saleor.site',
    'saleor.data_feeds',
    'saleor.blog',
    'saleor',

    # External apps
    'whitenoise.runserver_nostatic',
    'versatileimagefield',
    'babeldjango',
    'bootstrap3',
    'django_prices',
    'django_prices_openexchangerates',
    'emailit',
    'graphene_django',
    'mptt',
    'payments',
    'materializecssform',
    'rest_framework',
    'webpack_loader',
    'social_django',
    'django_countries',
    'selectable',
    'widget_tweaks',
    'raven.contrib.django.raven_compat',
    'markdown',
    'draceditor',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # Login via Google as an exemple, you can choose facebook, twitter as you like
    'allauth.socialaccount.providers.google',
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True
        },
        'saleor': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True
        }
    }
}

AUTH_USER_MODEL = 'userprofile.User'

LOGIN_URL = '/account/login/'

DEFAULT_COUNTRY = 'NZ'
DEFAULT_CURRENCY = 'NZD'
AVAILABLE_CURRENCIES = [DEFAULT_CURRENCY]

OPENEXCHANGERATES_API_KEY = os.environ.get('OPENEXCHANGERATES_API_KEY')

ACCOUNT_ACTIVATION_DAYS = 3

LOGIN_REDIRECT_URL = 'home'

GOOGLE_ANALYTICS_TRACKING_ID = os.environ.get('GOOGLE_ANALYTICS_TRACKING_ID')


def get_host():
    from saleor.site.utils import get_domain
    return get_domain()


PAYMENT_HOST = get_host

PAYMENT_MODEL = 'order.Payment'

PAYMENT_VARIANTS = {
    # 'paypal': ('payments.paypal.PaypalProvider', {
    #    'client_id': 'user@example.com',
    #    'secret': 'iseedeadpeople',
    #    'endpoint': 'https://api.sandbox.paypal.com',
    #    'capture': False}),
    # 'stripe': ('payments.stripe.StripeProvider', {
    #    'secret_key': 'sk_test_oRJZwJIj9rQll26rkQMI1JL8',
    #    'public_key': 'pk_test_326AMwouk28PVFhHPN50sGo4'}),
    # 'braintree': ('payments.braintree.BraintreeProvider', {
    #        'merchant_id': '4jyzcqr3zk4wsq9g',
    #        'public_key': 'zzyhpbzxzhtkfjyd',
    #        'private_key': '7d974e0f8f44f95bff0a9daee153c2a0',
    #        'sandbox': True}),
    'transfer': ('saleor.core.payment.NoProvider', {})
}

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

CHECKOUT_PAYMENT_CHOICES = [
    # ('stripe', 'Stripe'),
    # ('braintree', 'Braintree')
    ('transfer', 'Bank transfer or cash')
]

MESSAGE_TAGS = {
    messages.ERROR: 'danger'}

LOW_STOCK_THRESHOLD = 10
MAX_CART_LINE_QUANTITY = os.environ.get('MAX_CART_LINE_QUANTITY', 50)

PAGINATE_BY = 16

BOOTSTRAP3 = {
    'set_placeholder': False,
    'set_required': False,
    'success_css_class': '',
    'form_renderers': {
        'default': 'saleor.core.utils.form_renderer.FormRenderer',
    },
}

TEST_RUNNER = ''

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Amazon S3 configuration
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME')
AWS_MEDIA_BUCKET_NAME = os.environ.get('AWS_MEDIA_BUCKET_NAME')
AWS_QUERYSTRING_AUTH = ast.literal_eval(
    os.environ.get('AWS_QUERYSTRING_AUTH', 'False'))

if AWS_STORAGE_BUCKET_NAME:
    STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

if AWS_MEDIA_BUCKET_NAME:
    DEFAULT_FILE_STORAGE = 'saleor.core.storages.S3MediaStorage'
    THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

VERSATILEIMAGEFIELD_RENDITION_KEY_SETS = {
    'defaults': [
        ('product_gallery', 'crop__540x540'),
        ('product_gallery_2x', 'crop__1080x1080'),
        ('product_small', 'crop__60x60'),
        ('product_small_2x', 'crop__120x120'),
        ('product_list', 'crop__255x255'),
        ('product_list_2x', 'crop__510x510')]}

VERSATILEIMAGEFIELD_SETTINGS = {
    # Images should be pre-generated on Production environment
    'create_images_on_demand': ast.literal_eval(
        os.environ.get('CREATE_IMAGES_ON_DEMAND', 'True')),
}

PLACEHOLDER_IMAGES = {
    60: 'images/placeholder60x60.png',
    120: 'images/placeholder120x120.png',
    255: 'images/placeholder255x255.png',
    540: 'images/placeholder540x540.png',
    1080: 'images/placeholder1080x1080.png'
}

DEFAULT_PLACEHOLDER = 'images/placeholder255x255.png'

WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': not DEBUG,
        'BUNDLE_DIR_NAME': 'assets/',
        'STATS_FILE': os.path.join(PROJECT_ROOT, 'webpack-bundle.json'),
        'POLL_INTERVAL': 0.1,
        'IGNORE': [
            r'.+\.hot-update\.js',
            r'.+\.map']}}

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_EMAIL_VERIFICATION = 'none'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_SESSION_REMEMBER = False
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
ACCOUNT_FORMS = {
    'reset_password_from_key': 'saleor.userprofile.forms.SetPasswordForm'
}

AUTHENTICATION_BACKENDS = [
    'saleor.registration.backends.facebook.CustomFacebookOAuth2',
    'saleor.registration.backends.google.CustomGoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

SOCIAL_AUTH_PIPELINE = [
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
]

SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_USER_MODEL = AUTH_USER_MODEL
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id, email'}

LOGOUT_ON_PASSWORD_CHANGE = False

ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
SEARCHBOX_URL = os.environ.get('SEARCHBOX_URL')
BONSAI_URL = os.environ.get('BONSAI_URL')
# We'll support couple of elasticsearch add-ons, but finally we'll use single
# variable
ES_URL = ELASTICSEARCH_URL or SEARCHBOX_URL or BONSAI_URL or ''
if ES_URL:
    SEARCH_BACKENDS = {
        'default': {
            'BACKEND': 'saleor.search.backends.elasticsearch2',
            'URLS': [ES_URL],
            'INDEX': os.environ.get('ELASTICSEARCH_INDEX_NAME', 'storefront'),
            'TIMEOUT': 5,
            'AUTO_UPDATE': True},
        'dashboard': {
            'BACKEND': 'saleor.search.backends.dashboard',
            'URLS': [ES_URL],
            'INDEX': os.environ.get('ELASTICSEARCH_INDEX_NAME', 'storefront'),
            'TIMEOUT': 5,
            'AUTO_UPDATE': False}
    }
else:
    SEARCH_BACKENDS = {}

GRAPHENE = {
    'MIDDLEWARE': [
        'graphene_django.debug.DjangoDebugMiddleware'
    ],
    'SCHEMA': 'saleor.graphql.api.schema',
    'SCHEMA_OUTPUT': os.path.join(
        PROJECT_ROOT, 'saleor', 'static', 'schema.json')
}

SITE_SETTINGS_ID = 1

OPERATING_ONLY_IN_NZ = True

RAVEN_CONFIG = {
    'dsn': 'https://d940e0f607cb4168a59aaa3ea329cfbc:00cebc37f3fe4728ad1a7246d4f42290@sentry.io/147846',
}

# Global draceditor settings
# Input: string boolean, `true/false`
DRACEDITOR_ENABLE_CONFIGS = {
    str('imgur'): str('true'),  # to enable/disable imgur/custom uploader.
    str('mention'): str('false'),  # to enable/disable mention
    str('jquery'): str('false'),  # to include/revoke jquery (require for admin default django)
}

# Safe Mode
DRACEDITOR_MARKDOWN_SAFE_MODE = False  # default

# Markdownify
DRACEDITOR_MARKDOWNIFY_FUNCTION = 'draceditor.utils.markdownify'  # default
DRACEDITOR_MARKDOWNIFY_URL = '/draceditor/markdownify/'  # default

# Markdown extensions (default)
DRACEDITOR_MARKDOWN_EXTENSIONS = [
    'markdown.extensions.extra',
    'markdown.extensions.nl2br',
    'markdown.extensions.smarty',
    'markdown.extensions.fenced_code',

    # Custom markdown extensions.
    'draceditor.extensions.urlize',
    'draceditor.extensions.del_ins',  # ~~strikethrough~~ and ++underscores++
    'draceditor.extensions.mention',  # require for mention
    #'draceditor.extensions.emoji',  # require for emoji
]

# Markdown Extensions Configs
DRACEDITOR_MARKDOWN_EXTENSION_CONFIGS = {}

if os.getenv('IMG_LOC', 'local') == 'local':
    # Markdown urls
    DRACEDITOR_UPLOAD_URL = '/draceditor/uploader/'  # default

    # Upload to locale storage
    import time

    DRACEDITOR_UPLOAD_PATH = 'images/'
    DRACEDITOR_UPLOAD_URL = '/upload/'  # change to local uploader

    # Maximum Upload Image
    # 2.5MB - 2621440
    # 5MB - 5242880
    # 10MB - 10485760
    # 20MB - 20971520
    # 50MB - 5242880
    # 100MB 104857600
    # 250MB - 214958080
    # 500MB - 429916160
    MAX_IMAGE_UPLOAD_SIZE = 5242880  # 5MB
else:
    #Imgur API Keys
    DRACEDITOR_IMGUR_CLIENT_ID = '79e37b30d6e7e16'
    DRACEDITOR_IMGUR_API_KEY = '47716e290fb97bba3023b500411066923906c97e'

#!/bin/bash

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White


echo -e "${Yellow}${On_Purple}Remove old asset bundles before building a new one.${Color_Off}"
echo -e "${Green}${On_Black}rm -rf saleor/static/assets${Color_Off}"
rm -rf saleor/static/assets

echo -e "${Yellow}${On_Purple}build-prod will compile javascript, sass and give them a hash,${Color_Off}"
echo -e "${Yellow}${On_Purple} so the files can be served as static${Color_Off}"
echo -e "${Green}${On_Black}NODE_ENV=production yarn run build-assets${Color_Off}"
NODE_ENV=production yarn run build-assets

RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo -e "${Yellow}${On_Purple}Connect to the server and kill the current instance of the website${Color_Off}"
    echo -e "${Yellow}${On_Purple}(we use gunicorn to run)${Color_Off}"
    echo -e "${Green}${On_Black}ssh fzy@139.59.102.50 \"pkill -f gunicorn\"${Color_Off}"
    ssh fzy@139.59.102.50 "pkill -f gunicorn"
else
    echo -e "${White}${On_Red}FAILED!!!! Exit.${Color_Off}"
    exit
fi

echo -e "${Yellow}${On_Purple}Also remove all old assets from the remote site${Color_Off}"
echo -e "${Green}${On_Black}ssh fzy@139.59.102.50 \"rm -rf /home/fzy/workspace/ezpresso-saleor/saleor/static/assets\"${Color_Off}"
ssh fzy@139.59.102.50 "rm -rf /home/fzy/workspace/ezpresso-saleor/saleor/static/assets"

echo -e "${Yellow}${On_Purple}Make sure the server's code is up-to-date${Color_Off}"
echo -e "${Green}${On_Black}ssh fzy@139.59.102.50 \"cd /home/fzy/workspace/ezpresso-saleor; git pull\"${Color_Off}"
ssh fzy@139.59.102.50 "cd /home/fzy/workspace/ezpresso-saleor; git pull"

RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo -e "${Yellow}${On_Purple}Now copy the compile bundles over${Color_Off}"
    echo -e "${Yellow}${On_Purple} (We can compile them on the server, but it will be slow)${Color_Off}"
    echo -e "${Green}${On_Black}scp -r saleor/static/assets fzy@139.59.102.50:/home/fzy/workspace/ezpresso-saleor/saleor/static/${Color_Off}"
    scp -r saleor/static/assets fzy@139.59.102.50:/home/fzy/workspace/ezpresso-saleor/saleor/static/
else
    echo -e "${White}${On_Red}FAILED!!!! Exit.${Color_Off}"
    exit
fi

RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo -e "${Green}${On_Black}scp -r webpack-bundle.json fzy@139.59.102.50:/home/fzy/workspace/ezpresso-saleor/${Color_Off}"
    scp -r webpack-bundle.json fzy@139.59.102.50:/home/fzy/workspace/ezpresso-saleor/
else
    echo -e "${White}${On_Red}FAILED!!!! Exit.${Color_Off}"
    exit
fi

RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo -e "${Yellow}${On_Purple}Now, run gunicorn remotely ${Color_Off}"
    echo -e "${Green}${On_Black}ssh fzy@139.59.102.50 /home/fzy/workspace/ezpresso-saleor/post-deploy.sh${Color_Off}"
    ssh fzy@139.59.102.50 /home/fzy/workspace/ezpresso-saleor/post-deploy.sh
else
    echo -e "${White}${On_Red}FAILED!!!! Exit.${Color_Off}"
    exit
fi

RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo -e "${Yellow}${On_Purple}Now, remove the compiled bundles, rebuild the production version (no compile)${Color_Off}"
    echo -e "${Green}${On_Black}rm -rf assets/bundles${Color_Off}"
    rm -rf assets/bundles
    echo -e "${Green}${On_Black}NODE_ENV=development yarn build-assets${Color_Off}"
    NODE_ENV=development yarn build-assets
else
    echo -e "${White}${On_Red}FAILED!!!! Exit.${Color_Off}"
    exit
fi

RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo -e "${Black}${On_Green}ALL DONE${Color_Off}"
else
    echo -e "${White}${On_Red}FAILED!!!! Exit.${Color_Off}"
    exit
fi